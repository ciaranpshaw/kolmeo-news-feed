import React from 'react';

import NewsFeed from './components/NewsFeed';
import Header from './components/Header';

export default () => (
    <>
        <Header />
        <NewsFeed />
    </>
);