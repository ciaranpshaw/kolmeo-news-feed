export const NEWS_API_ENDPOINT = 'https://newsapi.org/v2';
export const NEWS_API_SECRET = process.env.REACT_APP_NEWS_API_SECRET;