import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.header`
    background: white;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    height: 80px;
    display: flex;
    justify-content: center;
`;

const Image = styled.img`
    height: 80px;
`;

const Header = () => (
    <Wrapper>
        <Image src="https://s3-ap-southeast-2.amazonaws.com/eliteagentlive/wp-content/uploads/2020/04/19163137/Kolmeo%402x.png" alt="Kolmeo Logo" />
    </Wrapper>
)

export default Header;