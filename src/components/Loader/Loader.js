import React from 'react';
import styled from 'styled-components';

import { ReactComponent as Loader } from './loader.svg';

const Wrapper = styled.div`
    transform: scale(0.3);
`;

export default () => (
    <Wrapper>
        <Loader />
    </Wrapper>
);