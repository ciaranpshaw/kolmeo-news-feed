import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Headline = styled.h3``;

const Author = styled.h4``;

const Summary = styled.p``;

const Wrapper = styled.a`
    width: calc(300px - 40px);
    box-shadow: 0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23);
    padding: 20px;
    margin-bottom: 30px;
    text-decoration: none;
    color: black;
`;

const NewsItem = ({ headline, author, summary, url }) => console.log(author) || (
    <Wrapper href={url}>
        <Headline>{headline}</Headline>
        <Author>{author}</Author>
        <Summary>{summary}</Summary>
    </Wrapper>
);

NewsItem.propTypes = {
    headline: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired,
    summary: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
};

export default NewsItem;