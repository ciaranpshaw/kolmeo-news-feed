import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const Select = styled.select`
    border: 0;
    border-bottom: 1px solid #cccccc;
    padding: 10px;
    padding-left: 0;
    font-size: 15px;
`;

const Option = styled.option``;

const Label = styled.label`
    margin-right: 10px;
`;

const Wrapper = styled.div`
    margin: 20px 0;
`;

const Dropdown = ({ options, onChange, value, label }) => (
    <Wrapper>
        {label && <Label htmlFor={label}>{label}</Label>}
        <Select onChange={onChange} value={value}>
            {options.map(({ value, label }) => <Option value={value}>{label}</Option>)}
        </Select>
    </Wrapper>
);

Dropdown.propTypes = {
    options: PropTypes.arrayOf(PropTypes.shape({
        label: PropTypes.string,
        value: PropTypes.string,
    })),
    onChange: PropTypes.func,
    value: PropTypes.string
};

Dropdown.defaultProps = {
    options: [],
    onChange: () => {},
};

export default Dropdown;