import React from 'react';
import styled from 'styled-components';
import axios from 'axios';

import NewsItem from '../NewsItem';
import { NEWS_API_ENDPOINT, NEWS_API_SECRET } from '../../constants';
import Loader from '../Loader';
import Dropdown from '../Dropdown';

const Feed = styled.div`
    margin: auto;
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
`;

const Wrapper = styled.section`
    margin: auto;
    max-width: calc((300px * 3) + (20px * 3));
`

class NewsFeed extends React.PureComponent {
    state = {
        stories: [],
        sources: [],
        source: 'bbc-news',
        country: 'au',

        isLoading: false,
        isError: false,
    };

    /** MAPPERS */
    _mapStory = story => console.log(story) || ({
        author: story.source.name,
        headline: story.title,
        summary: story.description,
        url: story.url
    });

    _mapSource = source => ({
        value: source.id,
        label: source.name
    });

    /** FETCH HELPERS */

    _fetchNewsItems = async () => {
        const { source } = this.state;

        this.setState({ isLoading: true });

        try {
            const { data } = await axios.get(`${NEWS_API_ENDPOINT}/top-headlines?sources=${source}&apiKey=${NEWS_API_SECRET}`);

            if (data && data.articles) {
                this.setState({
                    stories: data.articles.map(this._mapStory),
                    isLoading: false
                });
            }
        } catch (err) {
            this.setState({ isError: true, isLoading: false });
        }
    }

    _fetchNewsSources = () => new Promise(async res => {
        try {
            const { data } = await axios.get(`${NEWS_API_ENDPOINT}/sources?country=au&apiKey=${NEWS_API_SECRET}`);

            if (data && data.sources) {
                const sources = data.sources.map(this._mapSource);

                this.setState({ sources, source: sources[0].value }, res);
            }
        } catch (err) {
            this.setState({ isError: true }, res);
        }
    });
    
    _initialLoad = async () => {
        // Fetch news source first to ensure that the source is set
        await this._fetchNewsSources();
        this._fetchNewsItems();
    }
    
    /** CHANGE HANDLER */
    _handleSourceChange = e => {
        const source = e.target.value;

        this.setState({ source }, () => {
            this._fetchNewsItems();
        });
    }

    /** LIFECYLCE */

    componentDidMount() {
        this._initialLoad();
    }

    render() {
        const { stories, sources, source, isError, isLoading } = this.state;
        
        if (isError) return <>Error</>

        return (
            <Wrapper>
                <Dropdown label="Source" value={source} onChange={this._handleSourceChange} options={sources} />
                {isLoading ? 
                    <Loader /> :
                    <Feed>
                        {stories.map(story => <NewsItem {...story} />)}
                    </Feed>
                }
            </Wrapper>
        );
    }
};

export default NewsFeed;